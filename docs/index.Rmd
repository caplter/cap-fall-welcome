---
title: false
subtitle: false
author: false
date: false
output:
  xaringan::moon_reader:
    lib_dir: libs
    css: ["default", "assets/my-theme.css", "libs/remark-css/default-fonts.css", "hygge"]
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
    seal: false
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
```


class: middle, inverse

Effective long-term management and timely access to data emerging from LTER
research has been a cornerstone of the LTER Network since its inception. LTER
information managers had major roles in many of the ecoinformatics advances
that underlie the FAIR Data Principles (Findable, Accessible, Interoperable,
and Reusable) that we hear so much about today.

.footnote[
Groffman et al. 2019 [*Long Term Ecological Research Network Decadal Review Self Study*](https://lternet.edu/wp-content/uploads/2019/10/LTER_Self_Study_2019-10-04.pdf)
]

???

# LTER IM

---
class: middle

focus on archiving and publishing CAP LTER research data

* long-term monitoring and experiments
* student and other research

<br>

.center[
  <img src="assets/figures/data_processing.png" width="80%">
]

???

# CAP data streams


---
class: center, middle

<img src="assets/figures/CAPLTERlogo-color-icon.png" width="20%">
<img src="assets/figures/edi-logo.png" width="20%">
<img src="assets/figures/d1-logo.png" width="30%" align="top" vspace="30">

???

# data catalogs


---
class: middle

CAP LTER datasets archived in the Environmental Data Iniative (EDI) data repository (September 2020)

.center[
  <img src="assets/figures/number_datasets.png" width="60%">
]

~24K file downloads

???

# number datasets


---
class: middle, inverse

*basically, if the thought of redoing your analyses is terrifying then you are doing it wrong*  
<br>
J. Bryan

???

# redoing analyses


---
class: center, middle

.center[
  <img src="assets/figures/rdm.png" width="100%">
]

???

# education: RDM
